# Datatable Ajax Pagination

Author: Joseph Pahl

This repository provides an example of how to use the Ajax pagination feature of datatables.
This repository is set up to work in MAMP environments. 

To use this repository:

1) Clone this project, with the folder name of `dt`, to the htdocs folder of MAMP.
2) Start MAMP.
3) Point your browser at http://localhost/dt/


