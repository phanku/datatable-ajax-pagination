<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Get_data extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *        http://example.com/index.php/welcome
	 *    - or -
	 *        http://example.com/index.php/welcome/index
	 *    - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	}

	/**
	 * Returns the list of the columns for the datatable on the front end.
	 */
	public function get_columns()
	{
		$dt_columns = [];
		require(DATA_PATH . 'dt_data.php');
		echo json_encode($dt_columns);
	}

	/**
	 * Returns the data for the datatable based on the parameters provided by datatables.
	 */
	public function get_data()
	{
		$draw = intval($_GET['draw']);
		$start = intval($_GET['start']);
		$limit = intval($_GET['length']);

		$dt_data = [];
		require(DATA_PATH . 'dt_data.php');

		$out = new stdClass();
		$out->draw = $draw;
		$out->recordsTotal = count($dt_data);
		$out->recordsFiltered = count($dt_data);
		$out->data = array_slice($dt_data, $start, $limit);

		echo json_encode($out);
	}
}
