<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DT example</title>
	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<link type="text/css" rel="stylesheet" href="node_modules/datatables/media/css/jquery.dataTables.min.css" />
	<script src="node_modules/datatables/media/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<h1>Datatables with AJAX pagination example</h1>
	<hr>
	<p>See `welcome_message` view for the JS needed to trigger the ajax pagination in datatables.</p>
	<p>See `get_data` controller for how to respond to datatables ajax calls related to pagination.</p>
	<table id="dt" class="display" style="width:100%"></table>

	<script type="text/javascript">
		function fetchColumns() {
			return new Promise(((resolve, reject) => {
				$.ajax({
					url: "<?= site_url() ?>/get_data/get_columns/",
					type: 'GET',
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				})
			}))
		}

		$(document).ready(()=>{
			let columns = [];
			fetchColumns().then((data) => {
				$("#dt").DataTable({
					"processing": true,
					"serverSide": true,
					"pagination": true,
					"ajax": "index.php/get_data/get_data/",
					"columns": JSON.parse(data)
				});
			});
		});
	</script>
</body>
</html>
