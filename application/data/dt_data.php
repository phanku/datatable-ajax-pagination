<?php

$dt_columns = [];

$column = new stdClass();
$column->title = "ID";
$dt_columns[] = $column;

$column = new stdClass();
$column->title = "First Name";
$dt_columns[] = $column;

$column = new stdClass();
$column->title = "Last Name";
$dt_columns[] = $column;

$column = new stdClass();
$column->title = "Email";
$dt_columns[] = $column;

$column = new stdClass();
$column->title = "Gender";
$dt_columns[] = $column;

$column = new stdClass();
$column->title = "IP Address";
$dt_columns[] = $column;

$dt_data = array_map('str_getcsv', file(DATA_PATH . 'MOCK_DATA.csv'));
